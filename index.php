<?php

require_once 'vendor/autoload.php';

try {
	echo "Comenzando el proceso".PHP_EOL;

	$doc = new San\Estudioseijo\DocumentGenerator();

	$doc->addHeader('Encabezado - Estudioseijo');

	$doc->addTitle('Prueba técnica Estudioseijo', 1);

	$doc->addText('"Great achievement is usually born of great sacrifice, '
		        . 'and is never the result of selfishness." '
		        . '(Napoleon Hill)',
		    array('name' => 'Tahoma', 'size' => 10));

	$list = ['Elto 1', 'Elto 2', 'Elto 3', 'Elto 4'];
	$doc->addList($list);

	$doc->addLink('https://google.es', 'Enlace a Google');

	$doc->addFooter('Pie de página - Estudioseijo');

	ob_start();
	include './resources/content.html';
	$html = ob_get_clean();


	$doc->addNewSection();
	$doc->htmlToDoc($html);

	$doc->saveAsDocx('documento1');

	echo "Documento finalizado".PHP_EOL;

} catch (\Exception $e) {
	echo $e->getMessage();
}