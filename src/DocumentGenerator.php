<?php 

namespace San\Estudioseijo;

use \PhpOffice\PhpWord\PhpWord;
use \PhpOffice\PhpWord\IOFactory;
use \PhpOffice\PhpWord\Style\Font;
use \PhpOffice\PhpWord\Shared\Html;

class DocumentGenerator {

	private $phpWord;
	private $section;

	public function __construct() {
		$this->phpWord = new PhpWord();
		$this->phpWord->setDefaultFontName('Arial');
		$this->phpWord->setDefaultFontSize(12);
		$this->section = $this->phpWord->addSection();
	}

	public function addHeader($text, $fontStyle = array()) {
		$header = $this->section->addHeader();
		$header->addText($text, $fontStyle);
	}

	public function addFooter($text, $fontStyle = array()) {
		$footer = $this->section->addFooter();
		$footer->addPreserveText('Page {PAGE} of {NUMPAGES}.', null, ['alignment' => \PhpOffice\PhpWord\SimpleType\Jc::CENTER]);
		$footer->addText($text, $fontStyle);
		$footer->addLink('https://github.com/PHPOffice/PHPWord', 'PHPWord on GitHub');
	}

	public function addText($text, $fontStyle = array()) {
		$this->section->addText($text, $fontStyle);
		$this->addTextBreak();
	}

	public function addTitle($text, $depth) {
		$this->section->addTitle($text, $depth);
		$this->addTextBreak();
	}

	public function addList($items) {
		foreach($items as $item) {
			$this->section->addListItem($item);
		}
		$this->addTextBreak();
	}

	public function addLink($link, $text, $fontStyle = array()) {
		$this->section->addLink($link, $text, $fontStyle);
		$this->addTextBreak();
	}

	public function addNewSection() {
		$this->section = $this->phpWord->addSection();	
	}

	public function addTextBreak($n = 2) {
		$this->section->addTextBreak($n);
	}

	public function htmlToDoc($html) {
		Html::addHtml($this->section, $html, false, false);
	}

	

	public function saveAsDocx($filename) {
		$filenameExt = $this->addFilenameExtension($filename, 'docx');
		$this->saveAs('Word2007', $filenameExt);
	}

	public function saveAsOdt($filename) {
		$filenameExt = $this->addFilenameExtension($filename, 'odt');
		$this->saveAs('ODText', $filenameExt);
	}

	public function saveAsHtml($filename) {
		$filenameExt = $this->addFilenameExtension($filename, 'html');
		$this->saveAs('HTML', $filenameExt);
	}


	private function addFilenameExtension($filename, $extension) {
		return (empty($filename) ? 'helloWorld' : $filename).'.'.$extension;
	}

	private function saveAs($type, $filename) {
		$objWriter = IOFactory::createWriter($this->phpWord, $type);
		$objWriter->save($filename);
	}


}